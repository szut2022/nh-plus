package datastorage;

import model.Treatment;
import utils.DateConverter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class TreatmentDAO extends DAOimp<Treatment> {

    public TreatmentDAO(Connection conn) {
        super(conn);
    }

    /**
     * generates an <code>INSERT INTO</code>-Statement for a given {@link Treatment}
     *
     * @param treatment
     * @return String
     */
    @Override
    protected String getCreateStatementString(Treatment treatment) {
        return String.format(
                "INSERT INTO treatment (pid, cid, treatment_date, begin, end, description, remarks) VALUES " +
                        "(%d, %d, '%s', '%s', '%s', '%s', '%s')",
                treatment.getPid(), treatment.getCid(), treatment.getDate(),
                treatment.getBegin(), treatment.getEnd(), treatment.getDescription(),
                treatment.getRemarks());
    }

    /**
     * @param key
     * @return String
     */
    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM treatment WHERE tid = %d", key);
    }

    /**
     * gets the {@link Treatment} instance from a {@link ResultSet}
     *
     * @param result
     * @return Treatment
     * @throws SQLException
     */
    @Override
    protected Treatment getInstanceFromResultSet(ResultSet result) throws SQLException {
        LocalDate date = DateConverter.convertStringToLocalDate(result.getString(4));
        LocalTime begin = DateConverter.convertStringToLocalTime(result.getString(5));
        LocalTime end = DateConverter.convertStringToLocalTime(result.getString(6));
        Treatment m = new Treatment(result.getLong(1), result.getLong(2),
                result.getLong(3), date, begin, end,
                result.getString(7), result.getString(8));
        return m;
    }

    /**
     * generates a <code>select</code>-Statement for all {@link Treatment}s
     *
     * @return String
     */
    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM treatment";
    }

    /**
     * gets a list of all {@link Treatment}s from a {@link ResultSet}
     *
     * @param result
     * @return list of treatments
     * @throws SQLException
     */
    @Override
    protected ArrayList<Treatment> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Treatment> list = new ArrayList<Treatment>();
        while (result.next()) {
            list.add(getInstanceFromResultSet(result));
        }
        return list;
    }

    /**
     * generates a <code>UPDATE</code>-Statement for a {@link Treatment}
     *
     * @param treatment
     * @return the generated query
     */
    @Override
    protected String getUpdateStatementString(Treatment treatment) {
        return String.format("UPDATE treatment SET pid = %d, cid = %d, treatment_date ='%s', begin = '%s'," +
                        "end = '%s', description = '%s', remarks = '%s' WHERE tid = %d", treatment.getPid(), treatment.getCid(),
                treatment.getDate(), treatment.getBegin(), treatment.getEnd(), treatment.getDescription(),
                treatment.getRemarks(), treatment.getTid());
    }

    /**
     * generates a <code>delete</code> statement for the given key
     *
     * @param key treatment id
     * @return the generated query
     */
    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM treatment WHERE tid= %d", key);
    }

    /**
     * reads all {@link Treatment}s by a given {@link model.Patient} id
     *
     * @param pid patient id
     * @return list of treatments read
     * @throws SQLException
     */
    public List<Treatment> readTreatmentsByPid(long pid) throws SQLException {
        ArrayList<Treatment> list = new ArrayList<Treatment>();
        Statement st = conn.createStatement();
        ResultSet result = st.executeQuery(getReadAllTreatmentsOfOnePatientByPid(pid));
        list = getListFromResultSet(result);
        return list;
    }

    /**
     * reads all treatments of one {@link model.Patient} by the patient id
     *
     * @param pid the patient id
     * @return String
     */
    private String getReadAllTreatmentsOfOnePatientByPid(long pid) {
        return String.format("SELECT * FROM treatment WHERE pid = %d", pid);
    }

    /**
     * reads {@link Treatment}s by {@link model.Caregiver} id
     *
     * @param cid caregiver id
     * @return list of treatments read
     * @throws SQLException
     */
    public List<Treatment> readTreatmentsByCid(long cid) throws SQLException {
        ArrayList<Treatment> list = new ArrayList<Treatment>();
        Statement st = conn.createStatement();
        ResultSet result = st.executeQuery(getReadAllTreatmentsOfOneCaregiverByCid(cid));
        list = getListFromResultSet(result);
        return list;
    }

    /**
     * generates a <code>SELECT</code> statement to read all {@link Treatment}s of
     * one
     * {@link model.Caregiver} by its id
     *
     * @param cid the caregiver's id
     * @return the generated query
     */
    private String getReadAllTreatmentsOfOneCaregiverByCid(long cid) {
        return String.format("SELECT * FROM treatment WHERE cid = %d", cid);
    }

    /**
     * reads all {@link Treatment}s by a given {@link model.Patient} id and a given
     * {@link model.Caregiver} id
     *
     * @param pid patient id
     * @param cid caregiver id
     * @return list of treatments read
     * @throws SQLException
     */
    public List<Treatment> readTreatmentsByPidAndCid(long pid, long cid) throws SQLException {
        ArrayList<Treatment> list = new ArrayList<Treatment>();
        Statement st = conn.createStatement();
        ResultSet result = st.executeQuery(getReadAllTreatmentsByPidAndCid(pid, cid));
        list = getListFromResultSet(result);
        return list;
    }

    /**
     * generates a <code>SELECT</code> statement for all treatments by pid and cid
     *
     * @param pid patient id
     * @param cid caregiver id
     * @return the generated query
     */
    private String getReadAllTreatmentsByPidAndCid(long pid, long cid) {
        return String.format("SELECT * FROM treatment WHERE pid = %d AND cid = %d", pid, cid);
    }

    /**
     * generates a <code>delete</code>-Statement for a given key
     *
     * @param key for which a specific DELETE is to be created
     * @throws SQLException
     **/
    public void deleteByPid(long key) throws SQLException {
        Statement st = conn.createStatement();
        st.executeUpdate(String.format("Delete FROM treatment WHERE pid= %d", key));
    }
}