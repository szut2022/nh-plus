package datastorage;

/**
 * the DAO Factory in charge of creating instances of all implemented DAOs
 */
public class DAOFactory {
    private static DAOFactory instance;

    /**
     * @return DAOFactory
     */
    public static DAOFactory getDAOFactory() {
        if (instance == null) {
            instance = new DAOFactory();
        }
        return instance;
    }

    /**
     * creates a new <code>TreatmentDAO</code> instance
     *
     * @return treatment DAO
     */
    public TreatmentDAO createTreatmentDAO() {
        return new TreatmentDAO(ConnectionBuilder.getConnection());
    }

    /**
     * creates a new <code>PatientDAO</code> instance
     *
     * @return patient DAO
     */
    public PatientDAO createPatientDAO() {
        return new PatientDAO(ConnectionBuilder.getConnection());
    }

    /**
     * creates a new <code>CaregiverDAO</code> instance
     *
     * @return caregiver DAO
     */
    public CaregiverDAO createCaregiverDAO() {
        return new CaregiverDAO(ConnectionBuilder.getConnection());
    }
}