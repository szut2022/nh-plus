package datastorage;

import model.Caregiver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Implements the Interface <code>DAOImp</code>. Overrides methods to generate specific caregiver-SQL-queries.
 * This also contains the method for authentication
 */
public class CaregiverDAO extends DAOimp<Caregiver> {

    /**
     * constructs Object. Calls the Constructor from <code>DAOImp</code> to store the connection.
     *
     * @param conn
     */
    public CaregiverDAO(Connection conn) {
        super(conn);
    }

    /**
     * generates a <code>INSERT INTO</code>-Statement for a given caregiver
     *
     * @param caregiver for which a specific INSERT INTO is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getCreateStatementString(Caregiver caregiver) {
        return String.format(
                "INSERT INTO caregiver (firstname, surname, telephone, pass, email, loginfails)" +
                        "VALUES ('%s', '%s', '%s', '%s', '%s', '%s')",
                caregiver.getFirstName(),
                caregiver.getSurname(),
                caregiver.getTelephone(),
                caregiver.getPass(),
                caregiver.getEmail(),
                caregiver.getLoginfails(),
                caregiver.getEmail()
        );
    }

    /**
     * generates a <code>select</code>-Statement for a given key
     *
     * @param key for which a specific SELECTis to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM caregiver WHERE cid = %d", key);
    }

    /**
     * maps a <code>ResultSet</code> to a <code>caregiver</code>
     *
     * @param result ResultSet with a single row. Columns will be mapped to a caregiver-object.
     * @return caregiver with the data from the resultSet.
     */
    @Override
    protected Caregiver getInstanceFromResultSet(ResultSet result) throws SQLException {
        return new Caregiver(
                result.getInt(1),
                result.getString(2),
                result.getString(3),
                result.getString(4),
                result.getString(5),
                result.getString(6),
                result.getInt(7)
        );
    }

    /**
     * generates a <code>SELECT</code>-Statement for all caregivers.
     *
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM caregiver";
    }

    /**
     * maps a <code>ResultSet</code> to a <code>caregiver-List</code>
     *
     * @param result ResultSet with a multiple rows. Data will be mapped to caregiver-object.
     * @return ArrayList with caregivers from the resultSet.
     */
    @Override
    protected ArrayList<Caregiver> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Caregiver> list = new ArrayList<Caregiver>();
        while (result.next()) {
            list.add(getInstanceFromResultSet(result));
        }
        return list;
    }

    /**
     * generates a <code>UPDATE</code>-Statement for a given caregiver
     *
     * @param caregiver for which a specific update is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getUpdateStatementString(Caregiver caregiver) {
        return String.format(
                "UPDATE caregiver SET firstname = '%s', surname = '%s', telephone = '%s', " +
                        "email = '%s', loginfails = '%s' WHERE cid = %d",
                caregiver.getFirstName(),
                caregiver.getSurname(),
                caregiver.getTelephone(),
                caregiver.getEmail(),
                caregiver.getLoginfails(),
                caregiver.getCid()
        );
    }

    /**
     * generates a <code>delete</code>-Statement for a given key
     *
     * @param key for which a specific DELETE is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM caregiver WHERE cid=%d", key);
    }

    /**
     * reads a caregiver from the db by email address for the login
     *
     * @param email of the caregiver to be fetched
     * @return <code>Caregiver</code> object if found, otherwise null
     */
    public Caregiver readByEmail(String email) {
        try {
            Statement st = conn.createStatement();

            ResultSet result = st.executeQuery(String.format("SELECT * FROM caregiver WHERE email = '%s'", email));

            if (result.next()) {
                return getInstanceFromResultSet(result);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }
}
