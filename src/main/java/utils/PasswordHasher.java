package utils;

import de.mkammerer.argon2.Argon2Factory;
import de.mkammerer.argon2.Argon2;

/**
 * Argon2 password hashing utility
 */
public class PasswordHasher {

    /**
     * Hashes a password
     *
     * @param password
     * @return the generated hash
     */
    public static String create(String password) {
        Argon2 argon2 = Argon2Factory.create();
        return argon2.hash(10, 65536, 1, password.toCharArray());
    }

    /**
     * Verifies a password against a hash
     *
     * @param a password one
     * @param b password two
     * @return the result of the verification
     */
    public static boolean verify(String a, String b) {
        Argon2 argon2 = Argon2Factory.create();
        return argon2.verify(a, b.toCharArray());
    }
}
