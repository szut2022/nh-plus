package utils;

import javafx.scene.control.Alert;

/**
 * A utility for showing javafx alerts
 */
public class AlertHelper {
    /**
     * wrapper function to display an alert
     *
     * @param title the title of the alert
     * @param msg   the message of the alert
     */
    public static void showAlert(String title, String msg) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(title);
        alert.setContentText(msg);
        alert.showAndWait();
    }
}
