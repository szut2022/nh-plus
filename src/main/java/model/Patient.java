package model;

import org.json.simple.JSONObject;
import utils.DateConverter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Patients live in a NURSING home and are treated by nurses.
 */
public class Patient extends Person {
    private long pid;
    private LocalDate dateOfBirth;
    private String careLevel;
    private LocalDate endOfTreatment;
    private String roomnumber;
    private Boolean isLocked;
    private List<Treatment> allTreatments = new ArrayList<Treatment>();

    /**
     * constructs a patient from the given params.
     *
     * @param firstName      the first name of the patient
     * @param surname        the surname of the patient
     * @param dateOfBirth    the date of birth of the patient
     * @param careLevel      the care level of the patient
     * @param endOfTreatment the end of treatment for the patient
     * @param roomnumber     the roomnumber of the patient
     * @param isLocked       if the patient's personal data is locked or not
     */
    public Patient(String firstName, String surname, LocalDate dateOfBirth, String careLevel, LocalDate endOfTreatment,
                   String roomnumber, Boolean isLocked) {
        super(firstName, surname);
        this.dateOfBirth = dateOfBirth;
        this.careLevel = careLevel;
        this.endOfTreatment = endOfTreatment;
        this.roomnumber = roomnumber;
        this.isLocked = isLocked;
    }

    /**
     * constructs a patient from the given params.
     *
     * @param pid            the id of the patient
     * @param firstName      the first name of the patient
     * @param surname        the surname of the patient
     * @param dateOfBirth    the date of birth of the patient
     * @param careLevel      the care level of the patient
     * @param endOfTreatment the end of treatment for the patient
     * @param roomnumber     the roomnumber of the patient
     * @param isLocked       if the patient's personal data is locked or not
     */
    public Patient(long pid, String firstName, String surname, LocalDate dateOfBirth, String careLevel,
                   LocalDate endOfTreatment, String roomnumber, Boolean isLocked) {
        super(firstName, surname);
        this.pid = pid;
        this.dateOfBirth = dateOfBirth;
        this.careLevel = careLevel;
        this.endOfTreatment = endOfTreatment;
        this.roomnumber = roomnumber;
        this.isLocked = isLocked;
    }

    /**
     * @return patient id
     */
    public long getPid() {
        return pid;
    }

    /**
     * @return date of birth as a string
     */
    public String getDateOfBirth() {
        return dateOfBirth.toString();
    }

    /**
     * convert given param to a localDate and store as new <code>birthOfDate</code>
     *
     * @param dateOfBirth as string in the following format: YYYY-MM-DD
     */
    public void setDateOfBirth(String dateOfBirth) {
        LocalDate birthday = DateConverter.convertStringToLocalDate(dateOfBirth);
        this.dateOfBirth = birthday;
    }

    /**
     * @return careLevel
     */
    public String getCareLevel() {
        return careLevel;
    }

    /**
     * @param careLevel new care level
     */
    public void setCareLevel(String careLevel) {
        this.careLevel = careLevel;
    }

    /**
     * @return endOfTreatment
     */
    public String getEndOfTreatment() {
        return this.endOfTreatment.toString();
    }

    /**
     * convert given param to a localDate and store as new <code>birthOfDate</code>
     *
     * @param endOfTreatment as string in the following format: YYYY-MM-DD
     */
    public void setEndOfTreatment(String endOfTreatment) {
        LocalDate endOfTreatmentDate = DateConverter.convertStringToLocalDate(endOfTreatment);
        this.endOfTreatment = endOfTreatmentDate;
    }

    /**
     * @return roomNumber as string
     */
    public String getRoomnumber() {
        return roomnumber;
    }

    /**
     * @param roomnumber
     */
    public void setRoomnumber(String roomnumber) {
        this.roomnumber = roomnumber;
    }

    /**
     * @return isLocked
     */
    public Boolean getIsLocked() {
        return isLocked;
    }

    /**
     * @param isLocked
     */
    public void setIsLocked(Boolean isLocked) {
        this.isLocked = isLocked;
    }

    /**
     * adds a treatment to the treatment-list, if it does not already contain it.
     *
     * @param m Treatment
     * @return true if the treatment was not already part of the list. otherwise
     * false
     */
    public boolean add(Treatment m) {
        if (!this.allTreatments.contains(m)) {
            this.allTreatments.add(m);
            return true;
        }
        return false;
    }

    /**
     * @return string-representation of the patient
     */
    public String toString() {
        return "Patient" + "\nMNID: " + this.pid +
                "\nFirstname: " + this.getFirstName() +
                "\nSurname: " + this.getSurname() +
                "\nBirthday: " + this.dateOfBirth +
                "\nCarelevel: " + this.careLevel +
                "\nEnd of treatment: " + this.endOfTreatment +
                "\nRoomnumber: " + this.roomnumber +
                "\nIs locked: " + this.isLocked +
                "\n";
    }

    /**
     * @return json representation of the patient
     */
    public JSONObject toJsonObject() {
        JSONObject obj = new JSONObject();
        obj.put("id", this.pid);
        obj.put("name", this.getFirstName());
        obj.put("surname", this.getSurname());
        obj.put("birthdate", this.dateOfBirth);
        obj.put("carelevel", this.careLevel);
        obj.put("endoftreatment", this.endOfTreatment);
        obj.put("roomnumber", this.roomnumber);
        obj.put("islocked", this.isLocked.toString());
        return obj;
    }
}