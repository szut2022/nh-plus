package model;

import org.json.simple.JSONObject;

/**
 * Caregivers work in a NURSING home and treat patients.
 */
public class Caregiver extends Person {
    private long cid;
    private String telephone;
    private String pass;
    private String email;
    private long loginfails;

    /**
     * constructs a caregiver from the given params.
     *
     * @param firstName  the first name of the caregiver
     * @param surname    the name of the caregiver
     * @param telephone  a telephone number of the caregiver
     * @param pass       the password hash
     * @param email      the email address of the caregiver
     * @param loginfails the login fail counter
     */
    public Caregiver(String firstName, String surname, String telephone, String pass, String email, long loginfails) {
        super(firstName, surname);
        this.telephone = telephone;
        this.pass = pass;
        this.email = email;
        this.loginfails = loginfails;
    }

    /**
     * constructs a caregiver from the given params.
     *
     * @param cid        the id of the caregiver
     * @param firstName  the first name of the caregiver
     * @param surname    the name of the caregiver
     * @param telephone  a telephone number of the caregiver
     * @param pass       the password hash
     * @param email      the email address of the caregiver
     * @param loginfails the login fail counter
     */
    public Caregiver(long cid, String firstName, String surname, String telephone, String pass, String email,
                     long loginfails) {
        super(firstName, surname);
        this.cid = cid;
        this.telephone = telephone;
        this.pass = pass;
        this.email = email;
        this.loginfails = loginfails;
    }

    /**
     * @return caregiver id
     */
    public long getCid() {
        return cid;
    }

    /**
     * @return telephone number as a string
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * set telephone number
     *
     * @param telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return password hash as a string
     */
    public String getPass() {
        return pass;
    }

    /**
     * set password hash
     *
     * @param pass
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * @return email address of the caregiver
     */
    public String getEmail() {
        return email;
    }

    /**
     * set email address
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return get login fail counter, a value 3 or more will lead to the account
     * being locked
     */
    public long getLoginfails() {
        return loginfails;
    }

    /**
     * set login fail counter, a value 3 or more will lead to the account being
     * locked
     *
     * @param loginfails
     */
    public void setLoginfails(long loginfails) {
        this.loginfails = loginfails;
    }

    /**
     * This function intentionally omits printing the password hash
     *
     * @return string-representation of the caregiver
     */
    public String toString() {
        return "Caregiver" + "\nID: " + this.cid +
                "\nFirstname: " + this.getFirstName() +
                "\nSurname: " + this.getSurname() +
                "\nTelephone: " + this.telephone +
                "\nEmail: " + this.email +
                "\nFailed login attempts: " + this.loginfails +
                "\n";
    }

    /**
     * @return json representation of the caregiver
     */
    public JSONObject toJsonObject() {
        JSONObject obj = new JSONObject();
        obj.put("id", this.cid);
        obj.put("name", this.getFirstName());
        obj.put("surname", this.getSurname());
        obj.put("telephone", this.telephone);
        obj.put("email", this.email);
        return obj;
    }

}