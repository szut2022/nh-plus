package model;

import org.json.simple.JSONObject;
import utils.DateConverter;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Treatments are performed by a {@link Caregiver} onto a {@link Patient}
 */

public class Treatment {
    private long tid;
    private long pid;
    private long cid;
    private LocalDate date;
    private LocalTime begin;
    private LocalTime end;
    private String description;
    private String remarks;

    /**
     * constructs a treatment from the given params.
     *
     * @param pid         the id of the {@link Patient}
     * @param cid         the id of the {@link Caregiver}
     * @param date        the date of the treatment
     * @param begin       the begin time of the treatment (hh:mm)
     * @param end         the end time of the treatment (hh:mm)
     * @param description the description of the treatment
     * @param remarks     additional remarks about the treatment
     */
    public Treatment(long pid, long cid, LocalDate date, LocalTime begin,
                     LocalTime end, String description, String remarks) {
        this.pid = pid;
        this.cid = cid;
        this.date = date;
        this.begin = begin;
        this.end = end;
        this.description = description;
        this.remarks = remarks;
    }

    /**
     * constructs a treatment from the given params.
     *
     * @param tid         the id of the treatment
     * @param pid         the id of the {@link Patient}
     * @param cid         the id of the {@link Caregiver}
     * @param date        the date of the treatment
     * @param begin       the begin time of the treatment (hh:mm)
     * @param end         the end time of the treatment (hh:mm)
     * @param description the description of the treatment
     * @param remarks     additional remarks about the treatment
     */
    public Treatment(long tid, long pid, long cid, LocalDate date, LocalTime begin,
                     LocalTime end, String description, String remarks) {
        this.tid = tid;
        this.pid = pid;
        this.cid = cid;
        this.date = date;
        this.begin = begin;
        this.end = end;
        this.description = description;
        this.remarks = remarks;
    }

    /**
     * @return long
     */
    public long getTid() {
        return tid;
    }

    /**
     * @return long
     */
    public long getPid() {
        return this.pid;
    }

    public long getCid() {
        return this.cid;
    }

    /**
     * @return String
     */
    public String getDate() {
        return date.toString();
    }

    /**
     * @return String
     */
    public String getBegin() {
        return begin.toString();
    }

    /**
     * @return String
     */
    public String getEnd() {
        return end.toString();
    }

    /**
     * @param s_date
     */
    public void setDate(String s_date) {
        LocalDate date = DateConverter.convertStringToLocalDate(s_date);
        this.date = date;
    }

    /**
     * @param begin
     */
    public void setBegin(String begin) {
        LocalTime time = DateConverter.convertStringToLocalTime(begin);
        this.begin = time;
    }

    /**
     * @param end
     */
    public void setEnd(String end) {
        LocalTime time = DateConverter.convertStringToLocalTime(end);
        this.end = time;
    }

    /**
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return String
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return String
     */
    public String toString() {
        return "\nBehandlung" + "\nTID: " + this.tid +
                "\nPID: " + this.pid +
                "\nCID: " + this.cid +
                "\nDate: " + this.date +
                "\nBegin: " + this.begin +
                "\nEnd: " + this.end +
                "\nDescription: " + this.description +
                "\nRemarks: " + this.remarks + "\n";
    }

    /**
     * @return json representation of the treatment
     */
    public JSONObject toJsonObject() {
        JSONObject obj = new JSONObject();
        obj.put("id", this.tid);
        obj.put("patientid", this.pid);
        obj.put("caregiverid", this.cid);
        obj.put("date", this.date);
        obj.put("begin", this.begin);
        obj.put("end", this.end);
        obj.put("description", this.description);
        obj.put("remarks", this.remarks);
        return obj;
    }
}