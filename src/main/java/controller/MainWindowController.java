package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import model.Caregiver;
import model.Patient;
import model.Treatment;
import datastorage.CaregiverDAO;
import datastorage.DAOFactory;
import datastorage.PatientDAO;
import datastorage.TreatmentDAO;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import utils.AlertHelper;

import java.io.FileWriter;

/**
 * The <code>MainWindowController</code> contains the entire logic of the
 * sidebar navigation
 * It determines which data is displayed and how to react to events.
 */
public class MainWindowController {

    @FXML
    private BorderPane mainBorderPane;

    /**
     * Loads and displays the all patient view in the main window
     * Serves as onAction attribute for the patient sidebar button
     *
     * @param e the corresponding javafx <code>ActionEvent</code> (unused)
     */
    @FXML
    private void handleShowAllPatient(ActionEvent e) {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/AllPatientView.fxml"));
        try {
            mainBorderPane.setCenter(loader.load());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        AllPatientController controller = loader.getController();
    }

    /**
     * Loads and displays the all treatments view in the main window
     * Serves as onAction attribute for the treatment sidebar button
     *
     * @param e the corresponding javafx <code>ActionEvent</code> (unused)
     */
    @FXML
    private void handleShowAllTreatments(ActionEvent e) {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/AllTreatmentView.fxml"));
        try {
            mainBorderPane.setCenter(loader.load());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        AllTreatmentController controller = loader.getController();
    }

    /**
     * Loads and displays the all caregivers view in the main window
     * Serves as onAction attribute for the caregiver sidebar button
     *
     * @param e the corresponding javafx <code>ActionEvent</code> (unused)
     */
    @FXML
    private void handleShowAllCaregivers(ActionEvent e) {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/AllCaregiverView.fxml"));
        try {
            mainBorderPane.setCenter(loader.load());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        AllCaregiverController controller = loader.getController();
    }

    /**
     * exports all data to a "data.json" file which will be placed in the current working directory
     * Serves as onAction attribute for the export sidebar button
     *
     * @param e the corresponding javafx <code>ActionEvent</code> (unused)
     */
    @FXML
    private void handleJSONExport(ActionEvent e) {
        FileWriter writer;
        List<Patient> patientList = null;
        List<Caregiver> caregiverList = null;
        List<Treatment> treatmentList = null;

        PatientDAO pDAO = DAOFactory.getDAOFactory().createPatientDAO();
        CaregiverDAO cDAO = DAOFactory.getDAOFactory().createCaregiverDAO();
        TreatmentDAO tDAO = DAOFactory.getDAOFactory().createTreatmentDAO();

        try {
            patientList = pDAO.readAll();
            caregiverList = cDAO.readAll();
            treatmentList = tDAO.readAll();

        } catch (SQLException ex) {
            // TODO: handle exception
            ex.printStackTrace();
        }

        if (patientList == null || caregiverList == null || treatmentList == null) {
            AlertHelper.showAlert("Exportfehler", "Export fehlgeschlagen");
            return;
        }

        JSONArray pArr = new JSONArray();
        JSONArray cArr = new JSONArray();
        JSONArray tArr = new JSONArray();
        JSONObject jsonObj = new JSONObject();

        for (Patient p : patientList) {
            pArr.add(p.toJsonObject());
        }

        for (Caregiver c : caregiverList) {
            cArr.add(c.toJsonObject());
        }

        for (Treatment t : treatmentList) {
            tArr.add(t.toJsonObject());
        }

        jsonObj.put("patients", pArr);
        jsonObj.put("caregivers", cArr);
        jsonObj.put("treatments", tArr);

        try {
            writer = new FileWriter("data.json");
            writer.write(jsonObj.toJSONString());
            System.out.println("Successfully Copied JSON Object to File...");
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            AlertHelper.showAlert("Exportfehler", "Fehler beim Schreiben der Exportdatei");
            ex.printStackTrace();
            return;
        }
        AlertHelper.showAlert("Export erfolgreich", "Die Exportdatei wurde erfolgreich erstellt");
    }
}