package controller;

import datastorage.DAOFactory;
import datastorage.TreatmentDAO;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Caregiver;
import model.Patient;
import model.Treatment;
import utils.DateConverter;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * The <code>NewTreatmentController</code> contains the entire logic of the
 * new treatment view. It determines which data is displayed and how to react to
 * events.
 */
public class NewTreatmentController {
    @FXML
    private Label lblSurname;
    @FXML
    private Label lblFirstname;
    @FXML
    private Label lblCaregiver;
    @FXML
    private TextField txtBegin;
    @FXML
    private TextField txtEnd;
    @FXML
    private TextField txtDescription;
    @FXML
    private TextArea taRemarks;
    @FXML
    private DatePicker datepicker;

    private AllTreatmentController allTreatmentController;
    private Patient patient;
    private Caregiver caregiver;
    private Stage stage;

    /**
     * Initializes the corresponding fields. Is called as soon as the corresponding
     * FXML file is to be displayed.
     */
    public void initialize(AllTreatmentController controller, Stage stage, Patient patient, Caregiver caregiver) {
        this.allTreatmentController = controller;
        this.patient = patient;
        this.caregiver = caregiver;
        this.stage = stage;
        this.lblFirstname.setText(patient.getFirstName());
        this.lblSurname.setText(patient.getSurname());
        this.lblCaregiver.setText(String.format(
                "%s %s (Tel. %s)",
                caregiver.getFirstName(),
                caregiver.getSurname(),
                caregiver.getTelephone()
        ));
    }

    /**
     * handles an add-click-event. Creates a treatment and calls the create method in the {@link TreatmentDAO}
     * subsequently calls back <code>AllTreatmentController.handleFilters</code> to reload the table view with
     * the new data
     */
    @FXML
    public void handleAdd(){
        LocalDate date = this.datepicker.getValue();
        LocalTime begin = DateConverter.convertStringToLocalTime(txtBegin.getText());
        LocalTime end = DateConverter.convertStringToLocalTime(txtEnd.getText());
        String description = txtDescription.getText();
        String remarks = taRemarks.getText();
        Treatment treatment = new Treatment(
            patient.getPid(),
            caregiver.getCid(),
            date,
            begin,
            end,
            description,
            remarks
        );

        TreatmentDAO dao = DAOFactory.getDAOFactory().createTreatmentDAO();
        try {
            dao.create(treatment);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        allTreatmentController.handleFilters();
        stage.close();
    }

    /**
     * handles the closing of the new treatment window
     * Serves as onAction attribute for the corresponding button
     */
    @FXML
    public void handleCancel(){
        stage.close();
    }
}