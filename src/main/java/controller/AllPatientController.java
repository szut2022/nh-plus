package controller;

import datastorage.PatientDAO;
import datastorage.TreatmentDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import model.Patient;
import utils.DateConverter;
import datastorage.DAOFactory;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * The <code>AllPatientController</code> contains the entire logic of the
 * patient view. It determines which data is displayed and how to react to
 * events.
 */
public class AllPatientController {
    @FXML
    private TableView<Patient> tableView;
    @FXML
    private TableColumn<Patient, Integer> colID;
    @FXML
    private TableColumn<Patient, String> colFirstName;
    @FXML
    private TableColumn<Patient, String> colSurname;
    @FXML
    private TableColumn<Patient, String> colDateOfBirth;
    @FXML
    private TableColumn<Patient, String> colCareLevel;
    @FXML
    private TableColumn<Patient, String> colEndOfTreatment;
    @FXML
    private TableColumn<Patient, String> colRoom;

    @FXML
    Button btnDelete;
    @FXML
    Button btnAdd;
    @FXML
    Button btnShowLocked;
    @FXML
    Button btnShowAll;
    @FXML
    TextField txtSurname;
    @FXML
    TextField txtFirstname;
    @FXML
    TextField txtBirthday;
    @FXML
    TextField txtCarelevel;
    @FXML
    TextField txtEndOfTreatment;
    @FXML
    TextField txtRoom;

    private ObservableList<Patient> tableviewContent = FXCollections.observableArrayList();
    private PatientDAO patientDAO;
    private TreatmentDAO treatmentDAO;

    /**
     * Initializes the corresponding fields. Is called as soon as the corresponding
     * FXML file is to be displayed.
     */
    public void initialize() {
        this.patientDAO = DAOFactory.getDAOFactory().createPatientDAO();
        this.treatmentDAO = DAOFactory.getDAOFactory().createTreatmentDAO();

        manageEndOfTreatment();
        readAllAndShowInTableView();

        this.colID.setCellValueFactory(new PropertyValueFactory<Patient, Integer>("pid"));

        // CellValuefactory zum Anzeigen der Daten in der TableView
        this.colFirstName.setCellValueFactory(new PropertyValueFactory<Patient, String>("firstName"));
        // CellFactory zum Schreiben innerhalb der Tabelle
        this.colFirstName.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colSurname.setCellValueFactory(new PropertyValueFactory<Patient, String>("surname"));
        this.colSurname.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colDateOfBirth.setCellValueFactory(new PropertyValueFactory<Patient, String>("dateOfBirth"));
        this.colDateOfBirth.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colCareLevel.setCellValueFactory(new PropertyValueFactory<Patient, String>("careLevel"));
        this.colCareLevel.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colEndOfTreatment.setCellValueFactory(new PropertyValueFactory<Patient, String>("endOfTreatment"));
        this.colEndOfTreatment.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colRoom.setCellValueFactory(new PropertyValueFactory<Patient, String>("roomnumber"));
        this.colRoom.setCellFactory(TextFieldTableCell.forTableColumn());

        // Anzeigen der Daten
        this.tableView.setItems(this.tableviewContent);
    }

    /**
     * handles new firstname value
     * 
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditFirstname(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setFirstName(event.getNewValue());
        doUpdate(event);
    }

    /**
     * handles new surname value
     * 
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditSurname(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setSurname(event.getNewValue());
        doUpdate(event);
    }

    /**
     * handles new birthdate value
     * 
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditDateOfBirth(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setDateOfBirth(event.getNewValue());
        doUpdate(event);
    }

    /**
     * handles new carelevel value
     * 
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditCareLevel(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setCareLevel(event.getNewValue());
        doUpdate(event);
    }

    /**
     * handles new endOfTreatment value
     * 
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditEndOfTreatment(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setEndOfTreatment(event.getNewValue());
        doUpdate(event);
    }

    /**
     * handles new roomnumber value
     * 
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditRoomnumber(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setRoomnumber(event.getNewValue());
        doUpdate(event);
    }

    /**
     * updates a patient by calling the update-Method in the {@link PatientDAO}
     * 
     * @param t row to be updated by the user (includes the patient)
     */
    private void doUpdate(TableColumn.CellEditEvent<Patient, String> t) {
        try {
            patientDAO.update(t.getRowValue());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * calls readAll in {@link PatientDAO} and shows patients in the table
     */
    private void readAllAndShowInTableView() {
        this.tableviewContent.clear();

        List<Patient> allPatients;
        try {
            allPatients = patientDAO.readAll();
            allPatients.removeIf(Patient::getIsLocked);
            this.tableviewContent.addAll(allPatients);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * calls readAll in {@link PatientDAO} and shows all locked patients in the
     * table
     */
    private void readAllLockedAndShowInTableView() {
        this.tableviewContent.clear();
        List<Patient> allPatients;
        try {
            allPatients = patientDAO.readAll();
            allPatients.removeIf(patient -> !patient.getIsLocked());
            this.tableviewContent.addAll(allPatients);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * gets all from {@link PatientDAO}
     * checks if the current time is 30 years past each patients endOfTreatment, if so it deletes them.
     * if not it checks if the current time is past the endOfTreatment, if so it locks them
     */
    private void manageEndOfTreatment() {
        List<Patient> allPatients;
        try {
            allPatients = patientDAO.readAll();
            for (Patient patient : allPatients) {
                if (patient.getIsLocked() || patient.getEndOfTreatment().equals("")) {
                    continue;
                }
                LocalDate endOfTreatmentDate = DateConverter.convertStringToLocalDate(patient.getEndOfTreatment());
                if (endOfTreatmentDate.plus(30, ChronoUnit.YEARS).isBefore(LocalDate.now())) {
                    treatmentDAO.deleteByPid(patient.getPid());
                    patientDAO.deleteById(patient.getPid());
                } else if (endOfTreatmentDate.isBefore(LocalDate.now())) {
                    patient.setIsLocked(true);
                    patientDAO.update(patient);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * calls readAllLockedAndShowInTableView and serves as onAction
     * attribute for the corresponding button
     **/
    public void handleShowLocked() {
        this.tableView.setEditable(false);
        this.readAllLockedAndShowInTableView();
    }

    /**
     * calls readAllAndShowInTableView and serves as onAction attribute
     **/
    public void handleShowAll() {
        this.tableView.setEditable(true);
        this.readAllAndShowInTableView();
    }

    /**
     * handles a delete-click-event. Calls the delete methods in the
     * {@link PatientDAO} and {@link TreatmentDAO}
     */
    @FXML
    public void handleDeleteRow() {
        Patient selectedItem = this.tableView.getSelectionModel().getSelectedItem();
        if (selectedItem == null)
            return;
        try {
            treatmentDAO.deleteByPid(selectedItem.getPid());
            patientDAO.deleteById(selectedItem.getPid());
            this.tableView.getItems().remove(selectedItem);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * handles a add-click-event. Creates a patient and calls the create method in
     * the {@link PatientDAO}
     */
    @FXML
    public void handleAdd() {
        try {
            // TODO: run check if values are valid
            String surname = this.txtSurname.getText();
            String firstname = this.txtFirstname.getText();
            String birthday = this.txtBirthday.getText();
            LocalDate date = DateConverter.convertStringToLocalDate(birthday);
            String carelevel = this.txtCarelevel.getText();
            String endOfTreatment = this.txtEndOfTreatment.getText();
            LocalDate endoftreatmentlocaldate = DateConverter.convertStringToLocalDate(endOfTreatment);
            String room = this.txtRoom.getText();
            Patient p = new Patient(firstname, surname, date, carelevel, endoftreatmentlocaldate, room, false);
            patientDAO.create(p);
            readAllAndShowInTableView();
            clearTextfields();
        } catch (NumberFormatException e) {
            // TODO: popup
            e.printStackTrace();
        } catch (DateTimeException e) {
            // TODO: popup with error msg "invalid date (format)"
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO: add handler / error msg / ..
            e.printStackTrace();
        }
    }

    /**
     * removes content from all textfields
     */
    private void clearTextfields() {
        this.txtFirstname.clear();
        this.txtSurname.clear();
        this.txtBirthday.clear();
        this.txtCarelevel.clear();
        this.txtEndOfTreatment.clear();
        this.txtRoom.clear();
    }
}