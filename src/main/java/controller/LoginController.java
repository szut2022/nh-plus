package controller;

import datastorage.CaregiverDAO;
import datastorage.DAOFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Caregiver;
import utils.PasswordHasher;

import java.io.IOException;
import java.sql.SQLException;

/**
 * The <code>LoginController</code> contains the entire logic of the login view. It performs the authentication and
 */
public class LoginController {
    @FXML
    private BorderPane loginBorderPane;
    @FXML
    private TextField txtEmail;
    @FXML
    private PasswordField txtPass;

    private Stage primaryStage;

    /**
     * Initializes the corresponding fields. Is called as soon as the corresponding FXML file is to be displayed.
     */
    public void initialize(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    /**
     * saves a new firstname value
     * @param e javafx action event (unused but mandatory)
     */
    @FXML
    private void handleLogin(ActionEvent e) {
        long authResult = this.auth(txtPass.getText(), txtEmail.getText());
        if (authResult == 0) {
            this.onLoginSuccessful();
        } else {
            this.onLoginFailed(authResult);
        }
    }

    /**
     * saves a new firstname value
     * @param password the password to check
     * @param email the email to check
     * @return 0 on success, -1 for nonexistent caregivers, otherwise the login fail count of the caregiver
     */
    private long auth(String password, String email) {
        CaregiverDAO cDao = DAOFactory.getDAOFactory().createCaregiverDAO();
        Caregiver caregiver = cDao.readByEmail(email);

        if (caregiver == null) {
            return -1;
        }

        if (caregiver.getLoginfails() >= 3) {
            return caregiver.getLoginfails();
        }

        boolean valid = PasswordHasher.verify(caregiver.getPass(), password);

        long failCount = valid ? 0 : caregiver.getLoginfails() + 1;

        caregiver.setLoginfails(failCount);

        try {
            cDao.update(caregiver);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return failCount;
    }

    /**
     * handles the loading of the main window contents after a successful login attempt
     */
    private void onLoginSuccessful() {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/MainWindowView.fxml"));
        BorderPane pane = null;
        try {
            pane = loader.load();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        assert pane != null;
        Scene scene = new Scene(pane);

        this.primaryStage.setScene(scene);
        this.primaryStage.show();
    }

    /**
     * handles the loading of the main window contents after a successful login attempt
     * @param fails the number of failed login attempts for the caregiver or -1 if caregiver is unknown
     */
    private void onLoginFailed(long fails) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText("Login fehlgeschlagen!");
        if (fails >= 3) {
            alert.setContentText("Der Nutzer wurde gesperrt, bitte kontaktieren Sie einen Administrator um dies zu beheben");
        } else if(fails == -1) {
            alert.setContentText("Die eingegebenen Daten sind nicht bekannt");
        } else {
            alert.setContentText(String.format("Die eingegebenen Daten sind ungültig. Noch %d Versuche bis zur Sperrung", 3 - fails));
        }
        alert.showAndWait();
    }
}
