package controller;

import datastorage.CaregiverDAO;
import datastorage.PatientDAO;
import datastorage.TreatmentDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Caregiver;
import model.Patient;
import model.Treatment;
import datastorage.DAOFactory;
import utils.AlertHelper;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The <code>AllTreatmentController</code> contains the entire logic of the
 * treatment view. It determines which data is displayed and how to react to
 * events.
 */
public class AllTreatmentController {
    @FXML
    private TableView<Treatment> tableView;
    @FXML
    private TableColumn<Treatment, Integer> colID;
    @FXML
    private TableColumn<Treatment, Integer> colPid;
    @FXML
    private TableColumn<Treatment, Integer> colCid;
    @FXML
    private TableColumn<Treatment, String> colDate;
    @FXML
    private TableColumn<Treatment, String> colBegin;
    @FXML
    private TableColumn<Treatment, String> colEnd;
    @FXML
    private TableColumn<Treatment, String> colDescription;
    @FXML
    private ComboBox<String> patientComboBox;
    @FXML
    private ComboBox<String> caregiverComboBox;
    @FXML
    private Button btnNewTreatment;
    @FXML
    private Button btnDelete;
    @FXML
    private Button btnShowLocked;
    @FXML
    private Button btnShowAll;

    private boolean showLocked;

    private ObservableList<Treatment> tableviewContent = FXCollections.observableArrayList();
    private TreatmentDAO treatmentDAO;
    private PatientDAO patientDAO;
    private ObservableList<String> patientComboBoxData = FXCollections.observableArrayList();
    private ObservableList<String> caregiverComboBoxData = FXCollections.observableArrayList();
    private ArrayList<Patient> patientList;
    private ArrayList<Caregiver> caregiverList;

    /**
     * Initializes the corresponding fields. Is called as soon as the corresponding
     * FXML file is to be displayed.
     */
    public void initialize() {
        this.treatmentDAO = DAOFactory.getDAOFactory().createTreatmentDAO();
        this.patientDAO = DAOFactory.getDAOFactory().createPatientDAO();

        createComboBoxData();
        patientComboBox.setItems(patientComboBoxData);
        caregiverComboBox.setItems(caregiverComboBoxData);

        this.showLocked = false;
        reloadTableView();

        this.colID.setCellValueFactory(new PropertyValueFactory<Treatment, Integer>("tid"));
        this.colPid.setCellValueFactory(new PropertyValueFactory<Treatment, Integer>("pid"));
        this.colCid.setCellValueFactory(new PropertyValueFactory<Treatment, Integer>("cid"));
        this.colDate.setCellValueFactory(new PropertyValueFactory<Treatment, String>("date"));
        this.colBegin.setCellValueFactory(new PropertyValueFactory<Treatment, String>("begin"));
        this.colEnd.setCellValueFactory(new PropertyValueFactory<Treatment, String>("end"));
        this.colDescription.setCellValueFactory(new PropertyValueFactory<Treatment, String>("description"));
        this.tableView.setItems(this.tableviewContent);
    }

    /**
     * Resets the combo box selections, clears the table view and calls <code>showTableViewByLockStatus</code>
     */
    public void reloadTableView() {
        patientComboBox.getSelectionModel().select(0);
        caregiverComboBox.getSelectionModel().select(0);
        this.tableviewContent.clear();
        showTableViewByLockStatus();
    }

    /**
     * Reloads the table with the correct data for the <code>showLocked</code> state
     */
    public void showTableViewByLockStatus() {
        try {
            List<Treatment> allTreatments = treatmentDAO.readAll();
            for (Treatment treatment : allTreatments) {
                Patient p = patientDAO.read(treatment.getPid());

                if (this.showLocked == p.getIsLocked()) {
                    this.tableviewContent.add(treatment);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets showLocked to false‚ calls readAndShowInTableView
     * Serves as onAction attribute for the corresponding button
     **/
    public void handleShowLocked() {
        this.showLocked = true;
        this.tableView.setEditable(false);
        this.reloadTableView();
    }

    /**
     * calls readAndShowInTableView and serves as onAction attribute
     **/
    public void handleShowAll() {
        this.showLocked = false;
        this.tableView.setEditable(true);
        this.reloadTableView();
    }

    /**
     * initialization helper which loads all patient and caregiver surnames from the database and creates the
     * combo box data, which consists out of these surnames plus the option "alle"
     */
    private void createComboBoxData() {
        CaregiverDAO caregiverDAO = DAOFactory.getDAOFactory().createCaregiverDAO();

        this.patientComboBoxData.add("alle");
        this.caregiverComboBoxData.add("alle");

        try {
            patientList = (ArrayList<Patient>) patientDAO.readAll();
            for (Patient patient : patientList) {
                this.patientComboBoxData.add(patient.getSurname());
            }

            caregiverList = (ArrayList<Caregiver>) caregiverDAO.readAll();
            for (Caregiver caregiver : caregiverList) {
                this.caregiverComboBoxData.add(caregiver.getSurname());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads the appropriate data for the selected elements inside the combo boxes and displays them
     * Serves as onAction attribute for both combo boxes
     */
    @FXML
    public void handleFilters() {
        String p = this.patientComboBox.getSelectionModel().getSelectedItem();
        String c = this.caregiverComboBox.getSelectionModel().getSelectedItem();
        boolean showAllPatients = p.equals("alle");
        boolean showAllCaregivers = c.equals("alle");

        this.tableviewContent.clear();

        List<Treatment> allTreatments = null;
        Patient patient = searchPatientInList(p);
        Caregiver caregiver = searchCaregiverInList(c);

        try {
            if (showAllPatients && showAllCaregivers) {
                allTreatments = this.treatmentDAO.readAll();
            } else if (patient != null && showAllCaregivers) {
                allTreatments = treatmentDAO.readTreatmentsByPid(patient.getPid());
            } else if (caregiver != null && showAllPatients) {
                allTreatments = treatmentDAO.readTreatmentsByCid(caregiver.getCid());
            } else if (caregiver != null && patient != null) {
                allTreatments = treatmentDAO.readTreatmentsByPidAndCid(patient.getPid(), caregiver.getCid());
            }

            if (allTreatments != null) {
                this.tableviewContent.addAll(allTreatments);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param surname
     * @return the patient entity for the passed surname if found, otherwise null
     */
    private Patient searchPatientInList(String surname) {
        for (Patient patient : this.patientList) {
            if (patient.getSurname().equals(surname)) {
                return patient;
            }
        }
        return null;
    }

    /**
     * @param surname
     * @return the caregiver entity for the passed surname if found, otherwise null
     */
    private Caregiver searchCaregiverInList(String surname) {
        for (Caregiver caregiver : this.caregiverList) {
            if (caregiver.getSurname().equals(surname)) {
                return caregiver;
            }
        }
        return null;
    }

    /**
     * Deletes the selected row inside the table view if applicable
     * Serves as onAction attribute for the corresponding button
     */
    @FXML
    public void handleDelete() {
        int index = this.tableView.getSelectionModel().getSelectedIndex();
        if (index == -1)
            return; // nothing selected
        Treatment t = this.tableviewContent.remove(index);
        TreatmentDAO dao = DAOFactory.getDAOFactory().createTreatmentDAO();
        try {
            dao.deleteById(t.getTid());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Attempts to open up the new treatment view in an additional window in case both combo boxes are set to valid values
     * Serves as onAction attribute for the corresponding button
     */
    @FXML
    public void handleNewTreatment() {
        try {
            Patient patient = searchPatientInList(this.patientComboBox.getSelectionModel().getSelectedItem());
            if (patient == null) {
                AlertHelper.showAlert("Patient für die Behandlung fehlt!",
                        "Wählen Sie über die Combobox einen Patienten aus!");
                return;
            }

            Caregiver caregiver = searchCaregiverInList(this.caregiverComboBox.getSelectionModel().getSelectedItem());
            if (caregiver == null) {
                AlertHelper.showAlert("Pfleger für die Behandlung fehlt!",
                        "Wählen Sie über die Combobox einen Pfleger aus!");
                return;
            }

            newTreatmentWindow(patient, caregiver);
        } catch (NullPointerException e) {
            AlertHelper.showAlert("Fehler beim Erstellen der Behandlung!",
                    "Bitte überprüfen sie die eingegebenen Daten auf ihre Richtigkeit");
        }
    }

    /**
     * Calls <code>treatmentWindow</code> with the selected treatment entity if the user double-clicks a row in the table
     * Serves as onMouseClicked attribute for the table view
     */
    @FXML
    public void handleMouseClick() {
        tableView.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2 && (tableView.getSelectionModel().getSelectedItem() != null)) {
                int index = this.tableView.getSelectionModel().getSelectedIndex();
                Treatment treatment = this.tableviewContent.get(index);

                treatmentWindow(treatment);
            }
        });
    }

    /**
     * Opens up the new treatment view in an additional window
     *
     * @param patient   the patient entity to create a new treatment for
     * @param caregiver the caregiver entity to create a new treatment for
     */
    public void newTreatmentWindow(Patient patient, Caregiver caregiver) {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/NewTreatmentView.fxml"));
            AnchorPane pane = loader.load();
            Scene scene = new Scene(pane);
            // da die primaryStage noch im Hintergrund bleiben soll
            Stage stage = new Stage();

            NewTreatmentController controller = loader.getController();
            controller.initialize(this, stage, patient, caregiver);

            stage.setScene(scene);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Opens up the treatment detail view in an additional window
     *
     * @param treatment the treatment entity to display
     */
    public void treatmentWindow(Treatment treatment) {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/TreatmentView.fxml"));
            AnchorPane pane = loader.load();
            Scene scene = new Scene(pane);
            // da die primaryStage noch im Hintergrund bleiben soll
            Stage stage = new Stage();
            TreatmentController controller = loader.getController();

            controller.initializeController(this, stage, treatment);

            stage.setScene(scene);
            stage.setResizable(false);
            stage.showAndWait();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}