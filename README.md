# nh-plus

## Installation

Diese Anwendung benötigt eine aktuelle JDK und Maven Installation.

## Run

```bash
cd nh-plus/
mvn clean install
```

## Logindaten

```yaml
email: pavel@example.com
password: asdf
```

## Zusätzliche Features

- Sortierung nach normalen und gesperrten Daten bei Patienten sowie Behandlungen
- Password Hashing durch [Argon2](https://en.wikipedia.org/wiki/Argon2) Algorithmus
- JSON Export von Patienten, Behandlungen und Pflegern

## Testdokumentation

|                                                            | ❌ / ✅ |
| ---------------------------------------------------------- | ------- |
| **Userstory: Sperrung**                                    |         |
| TF_1 Autom. Sperrung                                       | ✅      |
| TF_2 Nachträgl. Sperrung                                   | ✅      |
| TF_3 Autom. Löschung nach 30 Jahren                        | ✅      |
| **Userstory: Pflegermodul**                                |         |
| TF_1 Alle Pflegekräfte anzeigen                            | ✅      |
| TF_2 Pflegekraft-Labels                                    | ✅      |
| TF_3 Combobox                                              | ✅      |
| TF_4 Pflegekräfte anlegen                                  | ✅      |
| TF_5 Pflegekräfte löschen                                  | ✅      |
| TF_6 Pflegekräfte editieren                                | ✅      |
| **Userstory: Vermögensstand**                              |         |
| TF_1 Kein Anzeigen                                         | ✅      |
| TF_2 Input entfernen                                       | ✅      |
| TF_3 Keine Speicherung                                     | ✅      |
| **Userstory: Login**                                       |         |
| TF_1 Erfolgreicher Login                                   | ✅      |
| TF_2 Verweigerung des Logins bei fehlerhaften Eingabedaten | ✅      |
| TF_3 Sperrung eines User                                   | ✅      |
| TF_4 Anmeldung als Startmaske                              | ✅      |
| TF_5 Anmeldung als Voraussetzung                           | ✅      |

## Authors

_Mark L. und Fabian R. (FA21C)_
